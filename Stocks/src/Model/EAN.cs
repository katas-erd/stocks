namespace Stocks.src.Model;

public class EAN
{
    private string value;

    private EAN(string value)
    {
        if (!value.All(char.IsLetterOrDigit))
            throw new ArgumentException("The EAN must contain alphanumeric characters only");

        if (value.Count() != 8)
            throw new ArgumentException("The EAN must contain exactly 8 characters");
        this.value = value;
    }

    public static EAN FromString(string value)
    {
        return new EAN(value);
    }

    public override bool Equals(object obj)
    {
        return obj != null && GetType() == obj.GetType() && value.Equals(((EAN)obj).value, StringComparison.OrdinalIgnoreCase);
    }

    public override int GetHashCode()
    {
        return value.GetHashCode();
    }
}
