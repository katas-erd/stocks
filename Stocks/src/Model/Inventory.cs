﻿using Stocks.src.Model;

namespace Stocks.src
{
    public partial interface Movement
    {
        public record Inventory(String Label, DateOnly Date, Quantity Quantity) : Movement
        {
            public Snapshot Compute(Snapshot listing, DateInterval constraint = null)
            {
                return Apply(listing, Quantity.Product, Date, constraint, (quantity) => Quantity);
            }
        }

        public static Movement DoAnInventory(String labelled, DateOnly on, Quantity quantity)
        {
            if (quantity.IsNegative())
            {
                throw new NegativeQuantityInInventory();
            }
            return new Inventory(labelled, on, quantity);
        }
    }
}
