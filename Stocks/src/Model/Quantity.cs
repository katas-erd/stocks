namespace Stocks.src.Model;

public record Quantity(Product Product, int Count)
{
    public bool IsPositive() => Count > 0;
    public bool IsNegative() => Count < 0;

    public static Quantity operator +(Quantity a, Quantity b)
        => new Quantity(a.Product, a.Count + b.Count);

    public static Quantity operator -(Quantity a, Quantity b)
        => new Quantity(a.Product, a.Count - b.Count);

    public static Quantity operator -(Quantity a) => new Quantity(a.Product, -a.Count);

}


public static class QuantityExt
{
    public static Quantity Of(this int count, Product product)
    {
        return new Quantity(product, count);
    }
}