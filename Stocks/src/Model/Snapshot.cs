﻿namespace Stocks.src.Model;

public class Snapshot
{

    List<Quantity> All;

    private Snapshot(List<Quantity> all)
    {
        All = all;
    }

    public static Snapshot Empty()
    {
        return new Snapshot(new List<Quantity>());
    }

    public Snapshot Replace(Quantity quantity)
    {
        All.RemoveAll(it => it.Product == quantity.Product);
        All.Add(quantity);
        return this;
    }

    public Snapshot InStockOnly()
    {
        return new Snapshot(All.Where(q => q.IsPositive()).ToList());
    }

    public Quantity Find(Product product)
    {
        return All.FirstOrDefault(quantity => quantity.Product == product, defaultValue: 0.Of(product));
    }

    public T Apply<T>(Func<IEnumerable<Quantity>, T> f)
    {
        return f(All);
    }

    internal Snapshot With(Quantity quantity)
    {
        All.Add(quantity);
        return this;
    }

    public override bool Equals(object? obj)
    {
        return obj is Snapshot other && other.All.SequenceEqual(All);
    }

    public override string? ToString()
    {
        return string.Join(",", All.Select(o => o.ToString()));
    }
}