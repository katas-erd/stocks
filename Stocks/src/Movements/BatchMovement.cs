﻿using Stocks.src.Model;

namespace Stocks.src
{
    public partial interface Movement
    {
        public record BatchMovement(DateOnly Date, String label, List<Movement> batch) : PostponableMovement
        {
            public Snapshot Compute(Snapshot listing, DateInterval constraint = null)
            {
                batch.ForEach(operation => operation.Compute(listing, constraint));
                return listing;
            }

            public bool IsEarlierOrToThatDate(DateOnly lastDate)
            {
                return batch.Select(o => o.Date).Any(d => d <= lastDate);
            }
        }

        public static PostponableMovement ApplyBatch(DateOnly on, String labelled, Movement batch, params Movement[] batches)
        {
            return new BatchMovement(on, labelled, Enumerable.Concat(new List<Movement>() { batch }, batches.ToList()).ToList());
        }
    }
}
