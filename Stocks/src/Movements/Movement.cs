﻿using Stocks.src.Model;

namespace Stocks.src
{
    public partial interface Movement
    {
        public DateOnly Date { get;  }
        Snapshot Compute(Snapshot listing, DateInterval? constraint = null);

        private static Snapshot Apply(Snapshot snapshot, Product product, DateOnly date, DateInterval constraint, Func<Quantity, Quantity> f)
        {
            if (constraint != null && !constraint.between(date))
            {
                return snapshot;
            }

            snapshot.Replace(f(snapshot.Find(product)));
            return snapshot;

        }
    }

    public record DateInterval(DateOnly From, DateOnly To)
    {
        public bool between(DateOnly date) {
            return From <= date && date <= To;
        }
    }
}
