﻿namespace Stocks.src
{
    public partial interface Movement
    {
        public interface PostponableMovement : Movement
        {
            bool IsEarlierOrToThatDate(DateOnly lastDate);
        }
    }
}
