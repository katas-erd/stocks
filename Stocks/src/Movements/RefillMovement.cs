﻿using Stocks.src.Model;

namespace Stocks.src
{
    public partial interface Movement
    {
        public record RefillMovement(DateOnly Date, Quantity Quantity) : PostponableMovement
        {
            public Snapshot Compute(Snapshot listing, DateInterval constraint = null)
            {
                return Apply(listing, Quantity.Product, Date, constraint, (quantity) => quantity + Quantity);
            }

            public bool IsEarlierOrToThatDate(DateOnly lastDate) => Date <= lastDate;
        
        }

        public static PostponableMovement Refill(Quantity quantity, DateOnly on)
        {
            return new RefillMovement(on, quantity);
        }
    }
}
