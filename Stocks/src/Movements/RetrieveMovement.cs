﻿using Stocks.src.Model;

namespace Stocks.src
{
    public partial interface Movement
    {
        public record RetrieveMovement(DateOnly Date, Quantity Quantity) : PostponableMovement
        {
            public Snapshot Compute(Snapshot listing, DateInterval constraint = null)
            {
                return Apply(listing, Quantity.Product, Date, constraint, (quantity) => quantity - Quantity);
            }

            public bool IsEarlierOrToThatDate(DateOnly lastDate) => Date <= lastDate;
        }

        public static PostponableMovement Delete(Quantity quantity, DateOnly on)
        {
            return new RetrieveMovement(on, quantity);
        }
    }
}
