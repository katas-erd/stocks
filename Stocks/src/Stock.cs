using System.Linq;
using Stocks.src.Model;
using static Stocks.src.Movement;

namespace Stocks.src;


public interface Clock
{
    DateOnly Today();
}
public class Stock
{

    private List<Movement>  operations = new List<Movement>();
    private Clock Clock;
    private Stock(Clock clock)
    {
        Clock = clock;
    }

    private Snapshot rebuild()
    {
        return operations.Aggregate(Snapshot.Empty(), (listing, operation) => operation.Compute(listing));
    }

    private Snapshot rebuild(DateOnly from, DateOnly to)
    {
        return operations.Aggregate(Snapshot.Empty(), (listing, operation) => operation.Compute(listing, new DateInterval(from, to)));
    }


    public static Stock Empty(Clock clock)
    {
        return new Stock(clock);
    }

    public Snapshot ProductsInStock()
    {
        return rebuild().InStockOnly();
    }

    public void Apply(PostponableMovement operation)
    {
        var lastInventory = operations.FindLast(o => o is Movement.Inventory);
        if (lastInventory != null && operation.IsEarlierOrToThatDate(lastInventory.Date)) {
            throw new InventoryAfterThatDate();
        }
        operations.Add(operation);
    }

    public void Inventory(String labelled, Quantity quantity)
    {
        operations.Add(Movement.DoAnInventory(labelled, Clock.Today(), quantity));
    }

    public int Count()
    {
        return ProductsInStock().Apply(all => all.Aggregate(0, (acc, value) => acc + value.Count));
    }

    public Quantity QuantityOf(Product product)
    {
        return ProductsInStock().Find(product);
    }

    public Quantity Variations(Product of, DateOnly from, DateOnly to)
    {
        return rebuild(from, to).Find(of);  
    }
}
