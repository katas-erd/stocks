﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stocks.src
{
    public abstract class UnauthorizedOperation: Exception
    {
    }

    public class NegativeQuantityInInventory : UnauthorizedOperation
    {
    }

    public class InventoryAfterThatDate : UnauthorizedOperation
    {
    }
}
