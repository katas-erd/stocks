﻿using FluentAssertions;
using Stocks.src;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stocks.tests
{
    using P = Personae;
    public class DateIntervalShould
    {

        [Fact(DisplayName ="return false when a date is before the interval")]
        public void Test0()
        {
            new DateInterval(P.Wednesday, P.Friday).between(P.Monday).Should().BeFalse();
        }

        [Fact(DisplayName = "return false when a date is after the interval")]
        public void Test1()
        {
            new DateInterval(P.Wednesday, P.Friday).between(P.Saturday).Should().BeFalse();
        }

        [Fact(DisplayName = "return true when a date is in the interval")]
        public void Test3()
        {
            new DateInterval(P.Monday, P.Friday).between(P.Wednesday).Should().BeTrue();
        }

        [Fact(DisplayName = "return true when a date is the first bound of the interval")]
        public void Test4()
        {
            new DateInterval(P.Monday, P.Friday).between(P.Monday).Should().BeTrue();
        }

        [Fact(DisplayName = "return true when a date is the second bound of the interval")]
        public void Test5()
        {
            new DateInterval(P.Wednesday, P.Friday).between(P.Friday).Should().BeTrue();
        }
    }
}
