using FluentAssertions;
using Stocks.src.Model;

namespace Stocks.tests;

public class EANShould
{
    [Fact(DisplayName = "Have alphanumeric characters")]
    void Test1()
    {
        Assert.Throws<ArgumentException>(() => EAN.FromString("ABCDEF!!!"));
    }

    [Fact(DisplayName = "Have 8 characters")]
    void Test2()
    {
        Assert.Throws<ArgumentException>(() => EAN.FromString("12345678910"));
        Assert.Throws<ArgumentException>(() => EAN.FromString("A"));
    }

    [Fact(DisplayName = "Be a value object")]
    void Test3()
    {
        EAN.FromString("12345678").Should().Be(EAN.FromString("12345678"));
        EAN.FromString("12345678").GetHashCode().Should().Be(EAN.FromString("12345678").GetHashCode());

        EAN.FromString("ABCDEFGH").Should().NotBe(EAN.FromString("12345678"));
        EAN.FromString("ABCDEFGH").GetHashCode().Should().NotBe(EAN.FromString("12345678").GetHashCode());
    }


}
