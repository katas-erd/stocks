using FluentAssertions;
using FluentAssertions.Execution;
using FluentAssertions.Primitives;
using Stocks.src;
using Stocks.src.Model;

namespace Stocks.tests;

using P = Personae;

public class GeneralFeatures: Personae
{

    

    [Fact(DisplayName = "Be created empty")]
    public void Test1()
    {
        var stock = CreateStock();

        stock.ProductsInStock().Should().BeEmpty();
    }

    [Fact(DisplayName = "add quantities of products (for example when purchasing from suppliers)")]
    public void Test3()
    {
        var stock = CreateStock();

        stock.Apply(Movement.Refill(3.Of(P.Milk), on: P.Monday));
        stock.Apply(Movement.Refill(7.Of(P.Milk), on: P.Monday));

        stock.ProductsInStock().Should().Contain(
            new Quantity(P.Milk, 10)
        );
    }

    [Fact(DisplayName = "delete quantities of products (for example when ordering customers)")]
    public void Test4()
    {
        var stock = CreateStock();

        stock.Apply(Movement.Refill(3.Of(P.Milk), on: P.Monday));
        stock.Apply(Movement.Delete(2.Of(P.Milk), on: P.Monday));

        stock.ProductsInStock().Should().Contain(
            new Quantity(P.Milk, 1)
        );
    }

    [Fact(DisplayName = "regularize the stock of a product (see inventory)")]
    public void Test5()
    {
        var stock = CreateStock();

        stock.Apply(Movement.Refill(3.Of(P.Milk), on: P.Monday));
        stock.Inventory(
                    labelled: "Inventory",
                    quantity: 10.Of(P.Milk));

        stock.ProductsInStock().Should().Contain(
            new Quantity(P.Milk, 10)
        );
    }

    [Fact(DisplayName = "know the products currently in stock (negative stocks are considered null)")]
    public void Test6()
    {
        var stock = CreateStock();

        stock.Apply(Movement.Refill(3.Of(P.Butter), on: P.Monday));
        stock.Apply(Movement.Refill(3.Of(P.Milk), on: P.Monday));
        stock.Apply(Movement.Delete(5.Of(P.Milk), on: P.Monday));

        stock.ProductsInStock().Should().ContainExactly(
            new Quantity(P.Butter, 3)
        );
    }

    [Fact(DisplayName = "to know the total number of products in the stock (negative stocks are considered as zero)")]
    public void Test7()
    {
        var stock = CreateStock();

        stock.Apply(Movement.Refill(7.Of(P.Cookie), on: P.Monday));
        stock.Apply(Movement.Refill(3.Of(P.Butter), on: P.Monday));
        stock.Apply(Movement.Refill(3.Of(P.Milk), on: P.Monday));
        stock.Apply(Movement.Delete(5.Of(P.Milk), on: P.Monday));
        stock.Apply(Movement.Delete(0.Of(P.Chocolate), on: P.Monday));

        stock.Count().Should().Be(10);
    }

    
    [Fact(DisplayName = "to know the current stock of a product")]
    public void Test8()
    {
        var stock = CreateStock();

        stock.Apply(Movement.Refill(7.Of(P.Cookie), on: P.Monday));

        stock.Apply(Movement.Refill(3.Of(P.Milk), on: P.Monday));
        stock.Apply(Movement.Delete(5.Of(P.Milk), on: P.Tuesday));
        stock.Apply(Movement.Refill(10.Of(P.Milk), on: P.Wednesday));

        stock.QuantityOf(P.Milk).Should().Be((3 - 5 + 10).Of(P.Milk));
    }

    [Fact(DisplayName = "to add several stock movements on several products on a date (but with a single caption)")]
    public void Test9()
    {
        var stock = CreateStock();
        stock.Apply(
            Movement.ApplyBatch(
                    on: P.Monday,
                    labelled: "New shipping",
                    batch: Movement.Refill(3.Of(P.Milk), on: P.Monday),
                           Movement.Refill(4.Of(P.Cookie), on: P.Monday),
                           Movement.Refill(5.Of(P.Butter), on: P.Tuesday),
                           Movement.Refill(5.Of(P.Milk), on: P.Tuesday)
            )


        );

        stock.ProductsInStock().Should().Contain(
            new Quantity(P.Milk, 8),
            new Quantity(P.Cookie, 4),
            new Quantity(P.Butter, 5)
        );
    }

    [Fact(DisplayName = "to know the stock variations of a product during any period (interval of dates)")]
    void Test10()
    {
        var stock = CreateStock();
        stock.Apply(Movement.Refill(5.Of(P.Butter), on: P.Tuesday));
        stock.Apply(Movement.Refill(3.Of(P.Milk), on: P.Wednesday));
        stock.Apply(Movement.Refill(4.Of(P.Cookie), on: P.Wednesday));
        stock.Apply(Movement.Refill(5.Of(P.Butter), on: P.Thursday));
        stock.Apply(Movement.Refill(5.Of(P.Milk), on: P.Friday));

        stock.Variations(of: P.Milk, from: P.Monday, to: P.Tuesday).Should().Be(0.Of(P.Milk));
        stock.Variations(of: P.Milk, from: P.Monday, to: P.Friday).Should().Be(8.Of(P.Milk));
    }
}


public static class Extensions
{
    public static SnapshotAssertions Should(this Snapshot instance)
    {
        return new SnapshotAssertions(instance);
    }
}


public class SnapshotAssertions :
    ReferenceTypeAssertions<Snapshot, SnapshotAssertions>
{
    public SnapshotAssertions(Snapshot instance) : base(instance)
    {
    }

    protected override string Identifier => "snapshot";

    public AndConstraint<SnapshotAssertions> BeEmpty()
    {
        Execute.Assertion
           .Given(() => Subject)
            .ForCondition(subject => subject.Equals(Snapshot.Empty()))
            .FailWith("Snapshot should be empty.");

        return new AndConstraint<SnapshotAssertions>(this);
    }

    internal AndConstraint<SnapshotAssertions> Contain(params Quantity[] quantities)
    {
        Execute.Assertion
           .Given(() => quantities.ToList())
            .ForCondition(quantities => quantities.All(q => Subject.Find(q.Product).Equals(q) ))
            .FailWith("Snapshot should contain {0}. But contains: {1}", quantities, Subject);

        return new AndConstraint<SnapshotAssertions>(this);
    }

    internal AndConstraint<SnapshotAssertions> ContainExactly(Quantity quantity)
    {
        Execute.Assertion
           .Given(() => Subject)
            .ForCondition(subject => subject.Equals(Snapshot.Empty().With(quantity)))
            .FailWith("Snapshot should contain {0}", quantity);

        return new AndConstraint<SnapshotAssertions>(this);
    }
}