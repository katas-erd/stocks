﻿using FluentAssertions;
using Stocks.src;
using Stocks.src.Model;

namespace Stocks.tests
{
    public class MovementShould: Personae
    {
        [Fact(DisplayName = "Be an inventory")]
        void Test1()
        {
            Movement.DoAnInventory(
                    labelled: "Inventory",
                    on: DateOnly.Parse("2020-01-01"),
                    quantity: 10.Of(Milk)
            ).Should().Be(
                Movement.DoAnInventory(
                    labelled: "Inventory",
                    on: DateOnly.Parse("2020-01-01"),
                    quantity: 10.Of(Milk)
                )
            );
        }

        [Fact(DisplayName= "Inventory movements cannot be negative.")]
        void Test2()
        {
            Action action = () => {
                Movement.DoAnInventory(
                    labelled: "Inventory",
                    on: DateOnly.Parse("2020-01-01"),
                    quantity: (-10).Of(Milk)
                );
            };

            action.Should().Throw<UnauthorizedOperation>();
        }
    }
}
