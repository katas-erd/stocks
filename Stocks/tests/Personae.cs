﻿using Stocks.src;
using Stocks.src.Model;

namespace Stocks.tests;

public abstract class Personae
{
    public static Product Milk = new Product(EAN.FromString("EAN00001"));
    public static Product Butter = new Product(EAN.FromString("EAN00002"));
    public static Product Cookie = new Product(EAN.FromString("EAN00003"));
    public static Product Chocolate = new Product(EAN.FromString("EAN00004"));

    public static DateOnly Monday = DateOnly.Parse("2023-10-23");
    public static DateOnly Tuesday = DateOnly.Parse("2023-10-24");
    public static DateOnly Wednesday = DateOnly.Parse("2023-10-25");
    public static DateOnly Thursday = DateOnly.Parse("2023-10-26");
    public static DateOnly Friday = DateOnly.Parse("2023-10-27");
    public static DateOnly Saturday = DateOnly.Parse("2023-10-28");
    public static DateOnly Sunday = DateOnly.Parse("2023-10-29");

    public static Stock CreateStock()
    {
        return Stock.Empty(new FakeClock(Monday));
    }

    public static Stock CreateStock(DateOnly todayIs)
    {
        return Stock.Empty(new FakeClock(todayIs));
    }
}

public record FakeClock(DateOnly TodayDate) : Clock
{

    public DateOnly Today()
    {
        return TodayDate;
    }
}
