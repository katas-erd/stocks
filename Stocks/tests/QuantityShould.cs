﻿using FluentAssertions;
using Stocks.src.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stocks.tests
{
    public class QuantityShould
    {
        private Product Milk = new Product(EAN.FromString("12345678"));

        [Fact(DisplayName= "Be a value object")]
        void Test0()
        {
            3.Of(Milk).Should().Be(3.Of(Milk));
        }

        [Fact(DisplayName = "It is possible to have negative quantities")]
        void Test1()
        {
            (new Quantity(Milk, -3).Count).Should().Be(-3);
            
            (-3).Of(Milk).Should().Be(-3.Of(Milk));
        }

        [Fact(DisplayName = "be negative or null")]
        void Test2()
        {
            (-3).Of(Milk).IsNegative().Should().BeTrue();
            3.Of(Milk).IsNegative().Should().BeFalse();
        }

        [Fact(DisplayName = "be positive")]
        void Test3()
        {
            3.Of(Milk).IsPositive().Should().BeTrue();
            (-3).Of(Milk).IsPositive().Should().BeFalse();
            0.Of(Milk).IsPositive().Should().BeFalse();
        }
    }
}
