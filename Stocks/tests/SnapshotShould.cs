﻿using FluentAssertions;
using Stocks.src.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stocks.tests
{
    using P = Personae;
    public class SnapshotsShould
    {
        [Fact(DisplayName="Find the quantity for the given product")]
        public void Test0()
        {
            Snapshot.Empty().With(3.Of(P.Milk))
                .Find(P.Milk)
                .Should().Be(3.Of(P.Milk));
        }
        
        [Fact(DisplayName = "Find a null quantity for the given product when it does not exist")]
        public void Test1()
        {
            Snapshot.Empty()
                .Find(P.Milk)
                .Should().Be(0.Of(P.Milk));
        }

        [Fact(DisplayName = "Replace a quantity")]
        public void Test2()
        {
            var sut = Snapshot.Empty().With(3.Of(P.Milk));

            sut.Replace(8.Of(P.Milk));

            sut.Find(P.Milk).Should().Be(8.Of(P.Milk));
        }

        [Fact(DisplayName = "Give only products in stock")]
        public void Test3()
        {
            var sut = Snapshot.Empty().With(3.Of(P.Milk)).With((-1).Of(P.Cookie));

            var result = sut.InStockOnly();
            result.Should().ContainExactly(3.Of(P.Milk));
        }
    }
}
