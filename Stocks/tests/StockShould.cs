﻿using FluentAssertions;
using Stocks.src;
using Stocks.src.Model;

namespace Stocks.tests
{
    public class StockShould: Personae
    {
        [Fact(DisplayName = "Refuse a movement at a date earlier than or equal to that of an inventory")]
        void Test0()
        {
            var stock = CreateStock(todayIs: Tuesday);

            stock.Apply(Movement.Refill(3.Of(Milk), on: Monday));

            stock.Inventory(
                    labelled: "Inventory",
                    quantity: 10.Of(Milk));

            Action action = () => { stock.Apply(Movement.Refill(3.Of(Milk), on: Monday)); };

            action.Should().Throw<UnauthorizedOperation>(); 
        }

        [Fact(DisplayName = "Adding a non-existing product to the stock creates the product")]
        public void Test1()
        {
            var stock = CreateStock();

            stock.Apply(Movement.Refill(3.Of(Milk), on: Monday));

            stock.ProductsInStock().Should().Contain(
                new Quantity(Milk, 3)
            );
        }

        [Fact(DisplayName = " If, when inserting a set of movements, one of the values ​​is not authorized (due to subsequent inventory),\r\n  all operations are rejected.")]
        public void Test2()
        {
            var dateOfInventory = Wednesday;
            var dateBeforeInventory = Monday;
            var dateAfterInventory = Friday;

            var stock = CreateStock(todayIs: dateOfInventory);

            stock.Apply(Movement.Refill(3.Of(Milk), on: Monday));

            stock.Inventory(
                    labelled: "Inventory",
                    quantity: 10.Of(Milk));

            Action action = () =>
            {
                stock.Apply(
                Movement.ApplyBatch(
                    on: Friday,
                    labelled: "New shipping",
                    batch: Movement.Refill(3.Of(Milk), on: Friday),
                           Movement.Refill(5.Of(Milk), on: dateBeforeInventory),
                           Movement.Refill(5.Of(Milk), on: dateAfterInventory)
            ));
            };

            action.Should().Throw<UnauthorizedOperation>();

        }
    }
}
